FROM python:3.12
RUN mkdir -p /opt/projeto
COPY . /opt/projeto
RUN pip install django python-decouple dj-database-url psycopg2-binary
WORKDIR /opt/projeto
RUN chmod +x entrypoint.sh
EXPOSE 8000
ENTRYPOINT [ "/opt/projeto/entrypoint.sh" ]
CMD [ "python", "manage.py", "runserver", "--noreload", "0.0.0.0:8000" ]
