from enum import Enum

class tipoResposta(Enum):
    Fim = 0
    existePosCorreta = 1
    existePosErrada = 2
    naoExiste = 3
    tentativaInvalida = 4