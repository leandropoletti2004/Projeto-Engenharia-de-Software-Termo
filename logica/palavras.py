from dataclasses import dataclass
from .tiposResposta import tipoResposta
from .estadoJogo import estadoJogo
from typing import List
@dataclass
class Palavra():
    nome: str
    listaResposta: list = None # type: ignore
    solved: bool = False
    
    def __post_init__(self):
        if self.listaResposta is None:
            self.listaResposta = []
            for i in self.nome:
                self.listaResposta.append(i)
    
    def checkAnswer(self, entrada: str):
        if len(entrada) == len(self.nome):
            # First check
            # for i in range(len(entrada)):
            #      #Function responsible for returning whether letter is right or not
            #      returnList.append(self._check(entrada[i], i))
            
            positions = [tipoResposta.naoExiste] * len(entrada)
            indexes = [None] * len(entrada)

            for i in range(len(entrada)):
                if entrada[i] == self.nome[i]:
                    positions[i] = tipoResposta.existePosCorreta
                    indexes[i] = i # type: ignore
            for i in range(len(entrada)):
                if positions[i] == tipoResposta.existePosCorreta:
                    continue
                else:
                    notused = []
                    for p in range(len(entrada)):
                        if p not in indexes:
                            notused.append(p)

                    for j in notused:
                        if entrada[i] == self.nome[j]:
                            positions[i] = tipoResposta.existePosErrada
                            notused.remove(j)
        
            returnList = positions.copy()
            # Solving Word
            self._trySolve(returnList)
            
            return returnList
        else:
            return tipoResposta.tentativaInvalida

    
    def _trySolve(self, resultado: list):
        resultado = set(resultado) # type: ignore
        if len(resultado) == 1:
            if tipoResposta.existePosCorreta in resultado:
                correta = True
            else:
                correta = False
            
            if correta:
                self.solved = True

    
    def getSolved(self) -> bool:
        return self.solved

class Jogo():
    
    def __init__(self, palavras: list, tentativas: int) -> None:
        self.state = estadoJogo.naoSolucionado
        self.listaPalavras = palavras
        self.tentativasRestantes = tentativas
    
    def enterAttempt(self, entrada):
        self.tentativasRestantes -= 1
        
        solved = []
        tipos = []
        for p in self.listaPalavras:
            tipos.append(p.checkAnswer(entrada))
            solved.append(p.getSolved())
        
        resumo = set(solved)
        if len(resumo) == 1 and True in resumo:
            self.state = estadoJogo.vencido
        elif self.tentativasRestantes == 0:
            
            self.state = estadoJogo.perdido
            
        return tipos
        
    def getWordState(self) -> List[bool]:
        result = []
        for p in self.listaPalavras:
            result.append(p.getSolved())
        return result
    
    def getState(self) -> estadoJogo:
        return self.state
