const tiles = document.querySelector(".tile-container");
const backspaceAndEnterRow = document.querySelector("#backspaceAndEnterRow");
const keyboardFirstRow = document.querySelector("#keyboardFirstRow");
const keyboardSecondRow = document.querySelector("#keyboardSecondRow");
const keyboardThirdRow = document.querySelector("#keyboardThirdRow");

const keysFirstRow = ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"];
const keysSdecondRow = ["A", "S", "D", "F", "G", "H", "J", "K", "L"];
const keysThirdRow = ["Z", "X", "C", "V", "B", "N", "M"];

const rows = parseInt(
  document.getElementById("num_tentativas_restantes").innerHTML
);
const columns = parseInt(document.getElementById("tamanho_palavra").innerHTML);
const id = parseInt(document.getElementById("id_sessao").innerHTML);
const tentativas_elemento = document.getElementById("num_tentativas_restantes");
let tentativas_restantes = tentativas_elemento.innerHTML;
let currentRow = 0;
let currentColumn = 0;
let jogo = 0;

// const termo = "VASCO";
// let termoMap = {}
// for (let index = 0; index < termo.length; index++) {
//     termoMap[termo[index]] = index
// }

const guesses = [];

let linhaAtual = 0;

for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
  guesses[rowIndex] = new Array(columns);
  const tileRow = document.createElement("div");
  tileRow.setAttribute("id", "row" + rowIndex);
  tileRow.setAttribute("class", "tile-row");
  for (let columnIndex = 0; columnIndex < columns; columnIndex++) {
    const tileColumn = document.createElement("div");
    tileColumn.setAttribute("id", "row" + rowIndex + "column" + columnIndex);
    tileColumn.setAttribute(
      "class",
      rowIndex == 0 ? "tile-column typing" : "tile-column disabled"
    );
    tileRow.append(tileColumn);
    guesses[rowIndex][columnIndex] = "";
  }
  tiles.append(tileRow);
}

const moveToNextRow = () => {
  var typingColumns = document.querySelectorAll(".typing");
  for (let index = 0; index < typingColumns.length; index++) {
    typingColumns[index].classList.remove("typing");
    typingColumns[index].classList.add("disabled");
  }
  currentRow++;
  currentColumn = 0;

  const currentRowEl = document.querySelector("#row" + currentRow);
  var currentColumns = currentRowEl.querySelectorAll(".tile-column");
  for (let index = 0; index < currentColumns.length; index++) {
    currentColumns[index].classList.remove("disabled");
    currentColumns[index].classList.add("typing");
  }
};

const checkGuess = () => {
  let resultado = [];

  const guess = guesses[currentRow].join("");
  if (guess.length != columns) {
    return alert("Quantidade de letras incorreta");
  }

  //TODO change to ajax request where it's based on the return of the api
  var currentColumns = document.querySelectorAll(".typing");
  // TODO change to dinamic
  $.post("/api/game_info", {
    update_database: 1,
    game_id: id,
    attempted_word: guess.toLowerCase(),
  }).done(function (data) {
    if (data.estado_jogo == "playing") {
      for (let i = 0; i < columns; i++) {
        resultado.push(data.estados[i]);
      }
    } else if (data.estado_jogo == "lost") {
      jogo = -1;
    } else {
      jogo = 1;
      for (let index = 0; index < columns; index++) {
        currentColumns[index].classList.add("right");
      }
      setTimeout(1000);
      alert("Voce ganhou!!!");
      setTimeout(2000);
      location.href = "/accounts/profile/";
    }

    tentativas_restantes--;
    tentativas_elemento.innerHTML = tentativas_restantes;

    if (jogo != 1)
      for (let index = 0; index < columns; index++) {
        //TODO Request to api sending the word and expecting an array with answer types
        if (resultado[index] == "wrong") {
          currentColumns[index].classList.add("wrong");
        } else {
          if (resultado[index] == "right") {
            currentColumns[index].classList.add("right");
          } else {
            currentColumns[index].classList.add("displaced");
          }
        }
      }
  });
  if (jogo == 0) {
    moveToNextRow();
  }
};

const heandleKeyboardOnClick = (key) => {
  if (currentColumn === columns) {
    return;
  }
  const currentTile = document.querySelector(
    "#row" + currentRow + "column" + currentColumn
  );
  currentTile.innerHTML = "<p>" + key + "</p>";

  guesses[currentRow][currentColumn] = key;
  currentColumn++;
};

const createKeyboardRow = (keys, keyboardRow) => {
  keys.forEach((key) => {
    var buttonElement = document.createElement("button");
    buttonElement.textContent = key;
    buttonElement.setAttribute("id", key);
    buttonElement.addEventListener("click", () => heandleKeyboardOnClick(key));
    keyboardRow.append(buttonElement);
  });
};

createKeyboardRow(keysFirstRow, keyboardFirstRow);
createKeyboardRow(keysSdecondRow, keyboardSecondRow);
createKeyboardRow(keysThirdRow, keyboardThirdRow);

const handleBackspace = () => {
  if (currentColumn === 0) {
    return;
  }

  currentColumn--;
  guesses[currentRow][currentColumn] = "";
  const tile = document.querySelector(
    "#row" + currentRow + "column" + currentColumn
  );
  tile.textContent = "";
};

const backspaceButton = document.createElement("button");
backspaceButton.addEventListener("click", handleBackspace);
backspaceButton.textContent = "<";
backspaceAndEnterRow.append(backspaceButton);

const enterButton = document.createElement("button");
enterButton.addEventListener("click", checkGuess);
enterButton.textContent = "ENTER";
backspaceAndEnterRow.append(enterButton);

let ignorar = [
  "Control",
  "AltGraph",
  "Alt",
  "Shift",
  "CapsLock",
  "Tab",
  "Alt",
  "Escape",
  "F1",
  "F2",
  "F3",
  "F4",
  "F5",
  "F6",
  "F7",
  "F8",
  "F9",
  "F10",
  "F11",
  "F12",
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
];

if (jogo == 0) {
  document.onkeydown = function (evt) {
    evt = evt || window.evt;
    if (evt.key === "Enter") {
      checkGuess();
    } else if (evt.key === "Backspace") {
      handleBackspace();
    } else if (!ignorar.includes(evt.key)) {
      heandleKeyboardOnClick(evt.key.toUpperCase());
    }
  };
} else if (jogo == -1) {
  alert("Jogo perdido");
} else {
  alert("Jogo ganho");
}
