from pyexpat import model
from venv import create
from django.db import models
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.


@admin.display(ordering="-id")
class PalavraInfo(models.Model):
    def __str__(self) -> str:
        return self.palavra
    palavra = models.CharField(max_length=12)


user_django = get_user_model()


class Atributes_User(models.Model):
    def __str__(self) -> str:
        return self.username.get_username()

    username = models.OneToOneField(user_django, on_delete=models.CASCADE)
    qntd_respondidas = models.IntegerField(default=0)
    level = models.IntegerField(default=0)


class Jogo_Sessoes(models.Model):
    def __str__(self) -> str:
        frase = str(self.usuario) + " em " + str(self.criado)
        return frase

    usuario = models.ForeignKey(Atributes_User, on_delete=models.CASCADE)
    # solved = models.ForeignKey(PalavraInfo, on_delete = models.CASCADE)
    palavras_usadas = models.ManyToManyField(PalavraInfo)
    additional_info = models.JSONField()
    tentativas_restantes = models.IntegerField(default=5)
    criado = models.DateField(auto_now_add=True)
    ganho = models.BooleanField(default=False)
    perdido = models.BooleanField(default=False)


@receiver(post_save, sender=user_django)
def on_user_create(instance, created, raw, **kwargs):
    if raw:
        return
    if not created:
        return
    Atributes_User.objects.create(username=instance)
