# Generated by Django 5.0.3 on 2024-05-15 02:58

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PalavraInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('palavra', models.CharField(max_length=12)),
            ],
        ),
        migrations.CreateModel(
            name='Atributes_User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('qntd_respondidas', models.IntegerField(default=0)),
                ('level', models.IntegerField(default=0)),
                ('username', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Jogo_Sessoes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('additional_info', models.JSONField()),
                ('tentativas_restantes', models.IntegerField(default=5)),
                ('criado', models.DateField(auto_now_add=True)),
                ('finalizado', models.BooleanField(default=False)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='palavra.atributes_user')),
                ('palavras_usadas', models.ManyToManyField(to='palavra.palavrainfo')),
            ],
        ),
    ]
