from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    # ------------------------ACCOUNT RELATED---------------------------
    path('', auth_views.LoginView.as_view(), name='login'),
    path('accounts/logout/', views.deslogar, name='logout'),
    
    path('accounts/create/', views.criar_usuario, name='criar_usuario'),

    # ------------------------GAME RELATED------------------------------
    path('game/form_inserir_palavra/', views.inserir_palavra, name='inserir_pergunta_test'),
    # path('tests/responder/<int:palavra_id>', views.verificar_resposta, name = "verificar_resposta"),
    path("accounts/profile/criar_jogo/", views.criar_jogo, name="criar_jogo"),
    path('accounts/profile/', view=views.list_games, name='listar_jogos'),
    path('accounts/profile/<int:id>', view=views.jogo_controller, name='jogo_controller'),
    path('api/game_info', view=views.retrieve_word_info, name='word_info'),

    # ------------------------TESTE URLS--------------------------------
    path('test/database', views.loading_database, name='database_test'),
]
