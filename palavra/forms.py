from django import forms

class palavra_resposta_form(forms.Form):
    entrada_palavra = forms.CharField(required=True, strip=True)

class inserir_palavra_form(forms.Form):
    palavra_inserir = forms.CharField(strip=True, required=True, label="Insira a nova palavra a ser adicionada")

class usuario_form(forms.Form):
    username = forms.CharField(required=True, strip=True, min_length=6, label="Insira o nome de usuario ")
    password = forms.CharField(required=True, strip=True)
    email = forms.EmailField(required=True, label="Email ")
    first_name = forms.CharField(required=True, strip=True, label="Primeiro nome ")
    last_name = forms.CharField(strip=True, label="Sobrenome", required=True)

class criar_jogo_form(forms.Form):
#    quantidade = forms.IntegerField(required=True, label="Insira a quantidade de palavras que o jogo deve conter")
    qntd_tentativas = forms.IntegerField(required=True, label="Insira a quantidade de tentativas que o jogo deve conter")