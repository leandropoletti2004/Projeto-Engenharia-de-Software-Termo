from palavra.models import PalavraInfo
from typing import List
from random import choice, sample
from logica.palavras import Jogo, Palavra


def get_palavra_list(qntd: int) -> List[PalavraInfo]:
    disponiveis = list(PalavraInfo.objects.all())
    selecionadas = sample(disponiveis, qntd)
    return selecionadas

def criar_sessao(palavras: List[PalavraInfo], tentativas) -> Jogo:
    lista_palavras = []
    for i in palavras:
        lista_palavras.append(Palavra(i.palavra.lower()))
    
    sessao = Jogo(lista_palavras, tentativas)

    return sessao
