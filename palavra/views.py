import http
from lib2to3.pytree import convert
from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model, login, logout
from .models import *
from .forms import *
import json
from logica.tiposResposta import tipoResposta
from logica.estadoJogo import estadoJogo
from .func.get_palavra import *

modelo_user = get_user_model()
# Create your views here.


def criar_usuario(request: HttpRequest):
    if request.method == "POST":
        formulario = usuario_form(request.POST)
        print(formulario.is_valid())
        print(formulario.errors)

        if formulario.is_valid():
            username = formulario.cleaned_data['username']
            password = formulario.cleaned_data['password']
            first_name = formulario.cleaned_data['first_name']
            last_name = formulario.cleaned_data['last_name']
            user_email = formulario.cleaned_data['email']

            print(username)
            print(password)
            t = modelo_user(username=username, first_name=first_name,
                            last_name=last_name, email=user_email)
            t.set_password(password)
            t.save()

            context = {
                "info": "Criado com sucesso"
            }
            # user = authenticate(username=username, password=password)
            login(request, t)
        else:
            context = {
                "info": "Falha na criação",
                "form": formulario,
            }

        return render(request, 'tests/additional_info.html', context)
    else:
        context = {
            'form': usuario_form()
        }
        return render(request, 'registration/sign_up.html', context)


def deslogar(request: HttpRequest):
    logout(request)
    context = {
        "info": "Deslogado com sucesso"
    }
    return render(request, 'tests/additional_info.html', context)


def loading_database(request):
    print(Atributes_User.objects.all())
    context = {
        'users': Atributes_User.objects.all()
    }
    return render(request, 'tests/test_database_connection.html', context)


def inserir_palavra(request: HttpRequest):
    if request.method == "POST":
        form = inserir_palavra_form(request.POST)
        if form.is_valid():
            palavra_inserir = form.cleaned_data['palavra_inserir']
            t = PalavraInfo(palavra=palavra_inserir)
            t.save()
            context = {
                "info": "Criado com sucesso"
            }
        else:
            context = {
                "info": "Falha na criação"
            }

        return render(request, 'tests/additional_info.html', context)

    else:
        context = {
            'form': inserir_palavra_form()
        }
    return render(request, 'tests/test_form_with_database.html', context)


def criar_jogo(request: HttpRequest):
    if request.user.is_authenticated == True:

        if request.method == "GET":
            context = {
                'form': criar_jogo_form()
            }
            return render(request, 'tests/test_game.html', context)
        elif request.method == "POST":
            configs_raw = criar_jogo_form(request.POST)
            if configs_raw.is_valid():
                #qntd_palavras = configs_raw.cleaned_data['quantidade']
                tentativas = configs_raw.cleaned_data['qntd_tentativas']
                palavras_tentadas = json.dumps({"palavras_tentadas": []})
                
                try:
                    selecionadas = get_palavra_list(1)
                except:
                    context = {
                        'info': "Quantidade de palavras registradas insuficientes!"
                    }
                    return render(request, 'tests/additional_info.html', context)
                user_atual = Atributes_User.objects.get(username=request.user)

                # palavras_escolhidas = {
                #     "palavras" : [x.palavra for x in selecionadas],
                # }

                # palavras_escolhidas_json = json.dumps(palavras_escolhidas)

                sessao = Jogo_Sessoes.objects.create(
                    usuario=user_atual,
                    tentativas_restantes=tentativas,
                    additional_info=palavras_tentadas
                )
                for i in selecionadas:
                    sessao.palavras_usadas.add(i)

                sessao.save()

                # redirect para harcoded url do jogo
                return redirect(list_games)
            else:
                context = {
                    "info": "Formulario incorreto"
                }
                return render(request, 'tests/additional.html', context)
        else:
            context = {
                "info": "Método da request não suportado"
            }
            return render(request, 'tests/additiona_infol.html', context)
    else:
        context = {
            "info": "Usuário não logado"
        }
        return render(request, 'tests/additional.html', context)


def list_games(request: HttpRequest):
    usuario_django = request.user
    if usuario_django.is_authenticated == False:
        context = {
            'info': "Você não esta logado!!"
        }
        return render(request, 'tests/additional_info2.html', context)

    usuario = Atributes_User.objects.get(username=usuario_django)
    games = Jogo_Sessoes.objects.filter(usuario=usuario)
    context = {
        'games': games,
        'user': usuario
    }
    return render(request, 'game/list_games.html', context)


def jogo_controller(request: HttpRequest, id):
    controller = Jogo_Sessoes.objects.get(id=id)
    palavras = list(controller.palavras_usadas.all())
    tamanho_palavra = len(palavras[0].palavra)
    # sessao = criar_sessao(palavras, controller.tentativas_restantes)
    lista_tentativas = json.loads(controller.additional_info)[
        'palavras_tentadas']
    if request.method == "GET":
        context = {
            'id': id,
            'palavras_tentadas': lista_tentativas,
            'restantes': controller.tentativas_restantes,
            'tamanho': tamanho_palavra
        }

    return render(request, 'game/game_page.html', context)


@csrf_exempt
def retrieve_word_info(request: HttpRequest):
    # data = json.loads(request.body)
    data = request.POST
    update_database = data['update_database']
    game_id = data['game_id']
    attempted_word = data['attempted_word']

    game_info = Jogo_Sessoes.objects.get(id=game_id)
    palavras_sessao = list(game_info.palavras_usadas.all())

    controller = criar_sessao(palavras_sessao, game_info.tentativas_restantes)
    if update_database:
        game_info.tentativas_restantes -= 1

    tipo = controller.enterAttempt(attempted_word)[0]

    game_state = controller.getState()
    if game_state == estadoJogo.perdido:
        game_info.perdido = True
        game_info.save()
        estados = {
            "estado_jogo": 'lost'
        }
        return JsonResponse(estados)
    if game_state == estadoJogo.vencido:
        game_info.ganho = True
        game_info.save()
        estados = {
            "estado_jogo": 'won'
        }
        return JsonResponse(estados)
    else:
        game_info.save()
        convertido = []
        for i in tipo:
            if i == tipoResposta.existePosCorreta:
                convertido.append('right')
                continue
            elif i == tipoResposta.existePosErrada:
                convertido.append('displaced')
                continue
            elif i == tipoResposta.naoExiste:
                convertido.append('wrong')
                continue
            elif i == tipoResposta.tentativaInvalida:
                convertido.append('invalid')
                break

        estados = {
            'estado_jogo': 'playing',
            'estados': [x for x in convertido]
        }

        return JsonResponse(estados)
