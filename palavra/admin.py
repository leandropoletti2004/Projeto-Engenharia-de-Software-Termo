from django.contrib import admin
from .models import *
# Register your models here.

class Jogos_SessoesInLine(admin.TabularInline):
    model = Jogo_Sessoes
    fields = ['id', 'tentativas_restantes', 'palavras_usadas']

class PalavraInfoInLine(admin.TabularInline):
    model = Jogo_Sessoes.palavras_usadas.through

class PalavraInfo_Admin(admin.ModelAdmin):
    readonly_fields = ["id"]
    list_display = ['id', 'palavra']
    
class Jogo_Sessoes_Admin(admin.ModelAdmin):
    fieldsets = [
        (
         "Informações sobre sessão:",
         {"fields": ['id','criado', 'tentativas_restantes', 'ganho', 'perdido'] }
        ),
        (
            "Usuário",
            {"fields": ['usuario']}
        ),
        (
            "Informações Adicionais",
            {'fields': ['additional_info']}
        )
    ]
    readonly_fields = ['id', 'criado']
    list_filter = ['criado']
    inlines = [PalavraInfoInLine]

class Atributes_UserAdmin(admin.ModelAdmin):
    actions_on_bottom = False
    fieldsets = [
        (
            "Nome usuário", {
            "fields": ["username"]
        }),
        (
            "Informações sobre a conta",{
                "fields": ["level","qntd_respondidas"]
            }
        )
        #Adicionar info sobre questoes
    ]

    inlines = [
        Jogos_SessoesInLine,
    ]


admin.site.register(Atributes_User, Atributes_UserAdmin)
admin.site.register(PalavraInfo, PalavraInfo_Admin)
admin.site.register(Jogo_Sessoes, Jogo_Sessoes_Admin)